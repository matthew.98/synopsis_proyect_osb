- Los proyectos de ejemplo del servicio retrunCycleBill no cambian respecto a las versiones anteriores 1.2.1/1.2.2/1.2.3
- En esta versión se inclyen dos nuevos proyectos de ejemplo:
	.Invoice-RetrieveCustomerInvoices_v2_example: Uso plantilla DSI en servicio mirror con traducción de errores a TOpen API
	.Invoice-RetrieveCustomerInvoices_example: Uso plantilla EXP en servicio mirror con traducción de errores a TOpen API cuando se invoca a la capa de orquestación en BPEL