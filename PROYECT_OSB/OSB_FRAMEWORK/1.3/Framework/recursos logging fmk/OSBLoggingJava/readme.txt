Proyecto de clases Java de apoyo para el log de ejecuciones dentro de un OSB/SOA Suite
--------------------------------------------------------------------------------------

***************
* Instalaci�n *
***************

Copiar librer�as de log4j2 al classpath del dominio sobre el que se ejecuta el OSB/SOA Suite.

Por ejemplo copiando las librer�as al directorio de librer�as comunes al dominio en %PATH_AL_DOMINIO%\lib 
(en una instalaci�n por defecto podr�a ser %MW_HOME%/user_projects/domains/base_domain/lib)

Las librerias a copiar son las distribuidas en el directorio libs: log4j-api*.jar, log4j-core-*.jar y 
disruptor-3.3.4.jar

*****************
* Configuraci�n *
*****************

La ubicaci�n y comportamiento de los logs se definen en un fichero de configuraci�n est�ndar de log4j2.

Se distribuye uno de ejemplo de nombre log4j2.xml en el directorio config.

Es necesario editar este fichero para al menos indicar el directorio en d�nde se generar�n los logs, para ello se debe 
editar la propiedad "log-path" definida al principio del fichero en la l�nea "<Property name="log-path">/logs/</Property>", 
cambiese "/logs/" por el directorio que se quiera. el directorio debe existir y el servidor de aplicaciones debe poder
escribir en �l.

Otras propiedades modificables del fichero de confiuraci�n son "execlog-filename", "execlogBPEL-filename" y "applog-filename" en d�nde se puede 
indicar el prefijo del nombre de fichero para el log de ejecuciones(OSB), log de ejecuciones(SOA Suite) y el log de aplicaci�n, respectivamente.

Una vez modificado el fichero de configuraci�n se debe copiar, seg�n sea un OSB o SOA Suite, al directorio /config/osb del dominio sobre el corre el OSB
(en una instalaci�n por defecto podr�a ser %MW_HOME%/user_projects/domains/base_domain/config/osb) o al directorio /config/soa-infra del dominio 
sobre el corre la SOA Suite (en una instalaci�n por defecto podr�a ser %MW_HOME%/user_projects/domains/base_domain/config/soa-infra).
Como alternativa a copiar el fichero de configuraci�n al directorio config/osb, se puede indicar la ubicaci�n del fichero
mediante una variable de entorno de la JVM de nombre "log4j.configurationFile", pasando entonces el par�metro a la JVM 
-Dlog4j.configurationFile=pathalficherodeconfiguracion.

***************
* Explotaci�n *
***************

La configuraci�n por defecto de los ficheros de logs hace que roten diariamente o por tama�o(1GB) y comprime los logs
rotados.

Cualquier cambio realizado a la configuraci�n de los logs entrar� en efecto en caliente como mucho 30 segundos despu�s 
de ser modificado el fichero de configuraci�n.

**************************
* Configuraci�n avanzada *
**************************

Al tratarse de un sistema de log est�ndar basado en log4j2, para realizar configuracioens avanzadas, se les remite a la 
documentaci�n del proyecto log4j2 en http://logging.apache.org/log4j/2.x/manual/
